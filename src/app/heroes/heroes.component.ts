import { Component, OnInit } from '@angular/core';
//import {HEROES} from '../mock-heros';//replaced with hero service
import {Hero} from '../hero'
import {HeroService} from '../hero.service'

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  title:string="heroes";
  heroes:Hero[]=[];// HEROES;
// selectedHero:Hero;
  constructor(private HeroService:HeroService) { }

  ngOnInit(): void {
    this.getHeroes();
  }
 /* onSelect(hero:Hero):void{
    this.selectedHero=hero;
  }*/
  getHeroes():void{
    //this.heroes=this.HeroService.getHeroes();
    this.HeroService.getHeroes().subscribe(heroes=>this.heroes=heroes)
  }
  addHero(heroName:string):void{
     heroName=heroName.trim();
     if(!heroName)return;
     this.HeroService.addHero({name:heroName} as Hero).subscribe(heroe=>this.heroes.push(heroe))
  }
  deleteHero(hero:Hero){
    this.HeroService.deleteHero(hero).subscribe(_=>this.heroes= this.heroes.filter(({id})=>id!==hero.id))
  }

}
