import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroService } from '../hero.service';
import {Hero} from '../hero'
import {Location} from '@angular/common'

@Component({
  selector: 'app-herodetail',
  templateUrl: './herodetail.component.html',
  styleUrls: ['./herodetail.component.css']
})
export class HeroDetailComponent implements OnInit {
     hero:Hero;
  constructor(private heroService:HeroService,private route:ActivatedRoute,private location:Location) { }
  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heroService.getHero(id)
      .subscribe(hero => this.hero = hero);
  }


  ngOnInit(): void {
    this.getHero();
  }
  goBack():void{
    this.location.back()
  }
  save():void{
    this.heroService.updateHero(this.hero).subscribe(() => this.goBack());
    ;
  }

}
