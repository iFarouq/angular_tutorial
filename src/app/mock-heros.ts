import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Dr Not Nice' },
  { id: 12, name: 'Batman' },
  { id: 13, name: 'Superman' },
  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dr IQ' },
  { id: 18, name: 'Dr LOW IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }
];